# Указываем, что мы хотим разворачивать окружение в AWS
provider "aws" {
  region = "us-east-1"
}


# Создаем запись в DNS для нашего домена
resource "aws_route53_record" "test-A" {
  zone_id = var.dns_zone_id
  name = "test"
  type = "A"

  alias {
    name = "${aws_elb.web.dns_name}"
    zone_id = "${aws_elb.web.zone_id}"
    evaluate_target_health = false
  }
}

#Создаем DNS-записи для мониторинкга
resource "aws_route53_record" "monitoring" {
  zone_id = var.dns_zone_id
  name = "monitoring"
  type = "A"
  ttl = "300"
  records = [aws_eip.mon_ip.public_ip]
}

resource "aws_route53_record" "grafana" {
  zone_id = var.dns_zone_id
  name = "grafana"
  type = "CNAME"
  ttl = "5"
  records = ["monitoring.rshalgochev-diploma.website"]
}

# Узнаём, какие есть Дата центры в выбранном регионе
data "aws_availability_zones" "available" {}

# Ищем образ с последней версией Ubuntu
data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Созаём правило, которое будет разрешать трафик к нашим серверам
resource "aws_security_group" "web" {
  name = "Dynamic Security Group"

  dynamic "ingress" {
    for_each = ["22", "80", "9100", "9080", "9096"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Web access for Application"
  }
}


# Создаём Launch Configuration. 

resource "aws_launch_configuration" "web" {
  name_prefix     = "Server-"
  image_id        = data.aws_ami.ubuntu.id
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.web.id]
  user_data       = file("user_data.sh")
  key_name = "id_rsa"
  lifecycle {
    create_before_destroy = true
  }
}


# AWS Autoscaling Group для указания, сколько нам понадобится инстансов 
resource "aws_autoscaling_group" "web" {
  name                 = "ASG-${aws_launch_configuration.web.name}"
  launch_configuration = aws_launch_configuration.web.name
  min_size             = 1
  max_size             = 1
  min_elb_capacity     = 1
  health_check_type    = "ELB"
  vpc_zone_identifier  = [aws_default_subnet.availability_zone_1.id, aws_default_subnet.availability_zone_2.id]
  load_balancers       = [aws_elb.web.name]

  dynamic "tag" {
    for_each = {
      Name   = "App-server"
      ENV    = "TEST"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
  
  lifecycle {
    create_before_destroy = true
  }
}

# Elastic Load Balancer проксирует трафик на наши сервера 
resource "aws_elb" "web" {
  name               = "WebServer-Highly-Available-ELB"
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  security_groups    = [aws_security_group.web.id]
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 80
    instance_protocol = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 10
    timeout             = 60
    target              = "HTTP:80/"
    interval            = 70
  }
  tags = {
    Name = "WebServer-Highly-Available-ELB"
  }
}

#Создаем объект IP-адреса для сервера мониторинга
resource "aws_eip" "mon_ip" {
  instance = aws_instance.monitoring.id
  tags = {
    Name  = "Monitoring IP"
  }
}

#Создаем инстанс под сервер мониторинга
resource "aws_instance" "monitoring" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.monitoring.id]
  key_name = "id_rsa"
  user_data = file("user_data.sh")
  tags = {
    AMI =  "${data.aws_ami.ubuntu.id}"
    ENV  = "MON"
  }

  lifecycle {
    create_before_destroy = true
  }

}

#Создаем правила доступа для нашего сервера мониторинга
resource "aws_security_group" "monitoring" {
  name        = "Monitoring Security Group for Ansible"
  description = "Security group for configuring monitoring via Ansible"


  dynamic "ingress" {
    for_each = ["80", "22", "3000", "9090", "9093", "9094", "9096", "3100", "9080"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Monitoring SecurityGroup for Ansible"
  }
}

# Созаём подсети в разных Дата центрах
resource "aws_default_subnet" "availability_zone_1" {
  availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_default_subnet" "availability_zone_2" {
  availability_zone = data.aws_availability_zones.available.names[1]
}

