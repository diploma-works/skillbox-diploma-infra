# Указываем, что мы хотим разворачивать окружение в AWS
provider "aws" {
  region = "us-east-1"
}


# Создаем запись в DNS для нашего домена
resource "aws_route53_record" "prod" {
  zone_id = var.dns_zone_id
  name = "prod"
  type = "CNAME"
  ttl = "300"
  records = [aws_elb.prod.dns_name]
}

# Узнаём, какие есть Дата центры в выбранном регионе
data "aws_availability_zones" "available" {}

# Ищем образ с последней версией Ubuntu
data "aws_ami" "ubuntu" {
  owners      = ["099720109477"]
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Созаём правило, которое будет разрешать трафик к нашим серверам
resource "aws_security_group" "prod" {
  name = "PROD Security Group"

  dynamic "ingress" {
    for_each = ["22", "80", "9100", "9080", "9096"]
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Access for PROD"
  }
}


# Создаём Launch Configuration. 

resource "aws_launch_configuration" "prod" {
  name_prefix     = "PROD-"
  image_id        = data.aws_ami.ubuntu.id
  instance_type   = "t2.micro"
  security_groups = [aws_security_group.prod.id]
  user_data       = file("user_data.sh")
  key_name = "id_rsa"
  lifecycle {
    create_before_destroy = true
  }
}


# AWS Autoscaling Group для указания, сколько нам понадобится инстансов 
resource "aws_autoscaling_group" "prod" {
  name                 = "ASG-${aws_launch_configuration.prod.name}"
  launch_configuration = aws_launch_configuration.prod.name
  min_size             = 1
  max_size             = 1
  min_elb_capacity     = 1
  health_check_type    = "ELB"
  vpc_zone_identifier  = [aws_default_subnet.availability_zone_1.id, aws_default_subnet.availability_zone_2.id]
  load_balancers       = [aws_elb.prod.name]

  dynamic "tag" {
    for_each = {
      Name   = "App-server"
      ENV    = "PROD"
    }
    content {
      key                 = tag.key
      value               = tag.value
      propagate_at_launch = true
    }
  }
  
  lifecycle {
    create_before_destroy = true
  }
}

# Elastic Load Balancer проксирует трафик на наши сервера 
resource "aws_elb" "prod" {
  name               = "PROD-ELB"
  availability_zones = [data.aws_availability_zones.available.names[0], data.aws_availability_zones.available.names[1]]
  security_groups    = [aws_security_group.prod.id]
  listener {
    lb_port           = 80
    lb_protocol       = "http"
    instance_port     = 80
    instance_protocol = "http"
  }
  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 10
    timeout             = 60
    target              = "HTTP:80/"
    interval            = 70
  }
  tags = {
    Name = "PROD-ELB"
  }
}


# Созаём подсети в разных Дата центрах
resource "aws_default_subnet" "availability_zone_1" {
  availability_zone = data.aws_availability_zones.available.names[0]
}

resource "aws_default_subnet" "availability_zone_2" {
  availability_zone = data.aws_availability_zones.available.names[1]
}

