variable "dns_zone_id" {
    default = "Z06553632Q0HPUK487B0M"
}

variable "ns_servers" {
    type = list(string)
    default = ["ns-695.awsdns-22.net", "ns-1831.awsdns-36.co.uk", "ns-1331.awsdns-38.org", "ns-358.awsdns-44.com"]
}
